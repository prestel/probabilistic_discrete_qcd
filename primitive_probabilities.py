
def getProbability(name):

    if name=="A":
      prob = 1./12.
    elif name=="B":
      prob = 1./12.
    elif name=="C":
      prob = 1./12.
    elif name=="D":
      prob = 1./12.
    elif name=="E":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="F":
      prob = 1./12.
    elif name=="G":
      prob = 1./12.
    elif name=="H":
      prob = 1./12.
    elif name=="I":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="J":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="K":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="L":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="M":
      prob = 1./12.
    elif name=="N":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="O":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="P":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="Q":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="R":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="S":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="T":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="U":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="V":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="W":
      prob = 1./12. * 1./2. * 1./2.
    elif name=="X":
      prob = 1./12. * 1./2. * 1./2.
    else:
      print ("Unknown structure name", name, ". Name has to be in [A...X]")

    return prob

