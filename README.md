# BayesMover: Inferring shower parameters from events 

We aim to produce code that allows to infer parameters of discrete-QCD based parton showers from fully differential event data. Let's see how far we get ;)

## Requirements

The code is written in Python 3. We plan to use JAX and NumPyro.

## Authors and acknowledgment

BayesMover is an extension of EventMover by Benjamin Nachman and Stefan Prestel, see the [preprint arXiv: ](https://arxiv.org/) for details.
