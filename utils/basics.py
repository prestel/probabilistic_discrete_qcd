
#import math as m

import sys

import jax.numpy as jnp
from vector import Vec4
from particle import Particle
from ltmatrix import LTmatrix

smearingfactor=10

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
  return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

def eq (a, b, rel_tol=1e-06, abs_tol=1e-10):
  return isclose(a,b, rel_tol, abs_tol)

def ne (a, b, rel_tol=1e-06, abs_tol=1e-10):
  return not isclose(a,b, rel_tol, abs_tol)

def lt (a, b):
  if isclose(a,b,1e-06,1e-10):
    return False
  return (a-b<0.0)

def gt (a, b):
  if isclose(a,b,1e-06,1e-10):
    return False
  return (a-b>0.0)

def le (a, b):
  return eq(a,b) or lt(a,b)

def ge (a, b):
  return  eq(a,b) or gt(a,b)

def convertArrayToEvent(nparray, ipmin=0, detach_last=True):
  event = []
  ipmax=len(nparray)
  if detach_last:
    ipmax-=1
  for ip in range(ipmin,ipmax):
    event.append(Particle(int(nparray[ip][0]),Vec4(nparray[ip][1],nparray[ip][2],nparray[ip][3],nparray[ip][4]),[0,0]))
  return event

def convertEventToArray(event,ipmin=0):
  nparray = jnp.array([jnp.array([event[pos].pid, event[pos].mom.E, event[pos].mom.px, event[pos].mom.py, event[pos].mom.pz]) for pos in range(ipmin,len(event))])
  return nparray

class RandomState:

    def __init__(self, pick_primitive_vec, pick_smear_kappa, pick_smear_y, pick_phi):
        self.rprimitive = pick_primitive_vec
        self.rkappa = pick_smear_kappa
        self.ry = pick_smear_y
        self.rphi = pick_phi
        self.ievent = 0
        return None

    def set_event_counter(self, ievent):
      self.ievent = ievent

    def get_next_rprimitive(self):
      return self.rprimitive[self.ievent]

    def get_next_rkappa(self,igluon):
      return self.rkappa[igluon][self.ievent]

    def get_next_ry(self,igluon):
      return self.ry[igluon][self.ievent]

    def get_next_rphi(self,igluon):
      return self.rphi[igluon][self.ievent]

#def discrete_inverse_trans(prob_vec, randomState):
#    U=randomState.get_next_rprimitive()
#    if le(U,prob_vec[0]):
#      return 0
#    else:
#      for i in range(1,len(prob_vec)+1):
#        if sum(prob_vec[0:i])<U and sum(prob_vec[0:i+1])>U:
#          return i

def discrete_inverse_trans(prob_vec, rn):
    U=rn
    if le(U,prob_vec[0]):
      return 0
    else:
      for i in range(1,len(prob_vec)+1):
        if sum(prob_vec[0:i])<U and sum(prob_vec[0:i+1])>U:
          return i

def max_offshellness(nparray):
    max_mass_now=0.
    for s in nparray:
      mass=abs(s[1]**2 -s[2]**2-s[3]**2-s[4]**2)
      if ne(s[0],0.):
        max_mass_now = max(max_mass_now,mass)
    return max_mass_now

def is_onshell(nparray, tol=0.1):
    evt = convertArrayToEvent(nparray)
    onshell=True
    for dp in evt:
      if abs(dp.mom.M2()) > tol:
        onshell=False
        break
    return onshell

def is_physical(nparray,ecm):
    evt = convertArrayToEvent(nparray)
    physical=True
    for dp in evt:
      if le(dp.mom.E, 0.) or gt(dp.mom.E,ecm):
        physical=False
        break
    return physical

def reshuffle(nparray, ecm):
   evt = convertArrayToEvent(nparray,0,False)
   psum = Vec4(0.,0.,0.,0.)
   for ip in range(len(evt)-1):
     p = evt[ip]
     mom = p.mom
     E = mom.E
     pabs = mom.P()
     p.mom = Vec4(E, E/pabs*mom.px, E/pabs*mom.py, E/pabs*mom.pz)
     psum+=p.mom
   align = LTmatrix()
   align.bst_p1_to_p2(psum, Vec4(ecm,0.,0.,0.))
   for ipp in range(len(evt)-1):
     evt[ipp].mom = align*evt[ipp].mom
   return convertEventToArray(evt)
